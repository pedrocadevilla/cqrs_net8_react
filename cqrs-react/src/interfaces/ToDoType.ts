import { TypeId } from "@helpers/types/TypeId";

export interface ToDoType {
    name: string;
    id: TypeId
}