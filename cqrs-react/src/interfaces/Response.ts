import { Error } from "@helpers/interfaces/Error";

export interface Response<T> {
    isSuccess: boolean,
    isFailure: boolean,
    error: Error,
    value: T
}