export interface ComponentList {
 component: () => JSX.Element;
 title: string;
 active: boolean;
}
