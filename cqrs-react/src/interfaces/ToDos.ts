import { ToDoType } from './ToDoType';
import { TypeId } from '@helpers/types/TypeId';

export interface ToDos {
  createdBy: string;
  description: string;
  id: TypeId;
  title: string;
  toDoType: ToDoType;
  updatedAt: string;
  createdAt: string;
}
