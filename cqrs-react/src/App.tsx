import React from 'react'
import './App.css'
import Layout from './components/layout/Layout'
function App() {

  return (
    <React.Fragment> 
      <Layout></Layout>     
    </React.Fragment>
  )
}

export default App
