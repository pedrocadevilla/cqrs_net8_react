import * as React from "react";
import ToDo from "@components/todo/ToDo";
import About from "@components/About";
import { useSEO } from "@hooks/useSEO";
import { ComponentList } from "@interfaces/ComponentList";
import NavBarComponent from "@components/layout/NavBarComponent";

const initialComponents: ComponentList[] = [
  { component: ToDo, title: "To Dos", active: true },
  { component: About, title: "About", active: false },
];

const Layout = () => {
  const [components, setComponent] = React.useState(initialComponents);
  useSEO({
    title: components.find((x) => x.active)!.title!,
    description: Date.now().toString(),
  });

  const displayComponent = () => {
    const FilteredComponent: ComponentList[] = components.filter(
      (x: ComponentList) => x.active,
    ) as ComponentList[];
    const CurrentComponent = FilteredComponent[0].component;
    return <CurrentComponent />;
  };

  return (
    <React.Fragment>
      <NavBarComponent
        setComponent={setComponent}
        components={components}
      ></NavBarComponent>
      <div className="container mx-auto">{displayComponent()}</div>
    </React.Fragment>
  );
};

export default Layout;
