import { Disclosure } from "@headlessui/react";
import { ComponentList } from "@interfaces/ComponentList";
import { classNames } from "@services/ClassName";

const NavBarComponent = ({
  setComponent,
  components,
}: {
  setComponent: React.Dispatch<React.SetStateAction<ComponentList[]>>;
  components: ComponentList[];
}) => {
  const handleTabChange = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    title: string,
  ) => {
    event?.preventDefault();
    const draftComponents = [...components];
    draftComponents.map((x) =>
      x.title === title ? (x.active = true) : (x.active = false),
    );
    setComponent(draftComponents);
  };
  return (
    <Disclosure as="nav" className="bg-gray-800">
      <>
        <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
          <div className="relative flex h-16 items-center justify-between">
            <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
              <div className="flex flex-shrink-0 items-center">
                <img
                  className="h-8 w-auto"
                  src="vite.svg"
                  alt="Your Company"
                />
              </div>
              <div className="hidden sm:ml-6 sm:block">
                <div className="flex space-x-4">
                  {components.map((item) => (
                    <a
                      key={item.title}
                      className={classNames(
                        item.active
                          ? "cursor-pointer bg-gray-900 text-white"
                          : "cursor-pointer text-gray-300 hover:bg-gray-700 hover:text-white",
                        "rounded-md px-3 py-2 text-sm font-medium",
                      )}
                      onClick={(e) => handleTabChange(e, item.title)}
                    >
                      {item.title}
                    </a>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </Disclosure>
  );
};

export default NavBarComponent;
