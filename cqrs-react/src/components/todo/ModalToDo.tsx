import { BaseSyntheticEvent, Fragment, useState } from "react";
import { Dialog, Listbox, Transition } from "@headlessui/react";
import { open, setModalOpen } from "@signals/CreateModalToDo";
import { CheckIcon } from "@heroicons/react/20/solid";
import { ChevronUpDownIcon } from "@heroicons/react/20/solid";
import { setToDos, toDoTypes, toDos } from "@signals/ToDoList";
import { ToDos } from "@interfaces/ToDos";
import { ToDoType } from "@interfaces/ToDoType";
import { TypeId } from "@helpers/types/TypeId";
import { createToDo, updateToDo } from "@services/ToDos";
import { initialTodo } from "@signals/InitialToDo";
const ModalToDo = () => {
  const [toDo, setToDo] = useState(structuredClone(initialTodo.value));
  const formHandler = (e: BaseSyntheticEvent) => {
    const name = e.target.name;
    const value = e.target.value;
    setToDo({ ...toDo, [name]: value });
  };
  const dropDownHandler = (e: ToDoType) => {
    setToDo({ ...toDo, toDoType: e });
  };
  const submitForm = async (event: Event | undefined) => {
    event?.preventDefault();
    if (!initialTodo.value.id) {
      const toDoId: TypeId | null = await createToDo(toDo as ToDos);
      const draftToDos = [...toDos.value];
      if (toDoId !== null) {
        toDo.id = toDoId;
        draftToDos.push(toDo as ToDos);
        setToDos(draftToDos);
        setModalOpen(false);
      }
      return;
    }
    const draftToDos = [...toDos.value];
    const todoIndex = draftToDos.findIndex(
      (x) => x.id === initialTodo.value.id,
    );
    draftToDos[todoIndex] = toDo as ToDos;
    const update: TypeId | null = await updateToDo(draftToDos[todoIndex]);
    if (update !== null) {
      setToDos(draftToDos);
      setModalOpen(false);
    }
    return;
  };
  return (
    <Transition.Root show={open.value} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        onClose={() => setModalOpen(false)}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel
                as="form"
                onSubmit={() => submitForm(event)}
                className="relative transform overflow-ellipsis rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-xl"
              >
                <div className="px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                  <div className="grid grid-cols-1">
                    <h2 className="text-base font-semibold leading-7 text-gray-900">
                      Create ToDo
                    </h2>
                    <div className="mt-4 grid grid-cols-2 gap-x-6 gap-y-2 sm:grid-cols-6">
                      <div className="sm:col-span-3">
                        <label
                          htmlFor="title"
                          className="block text-sm font-medium leading-6 text-gray-900"
                        >
                          Title
                        </label>
                        <input
                          type="text"
                          name="title"
                          id="title"
                          onChange={formHandler}
                          value={toDo.title}
                          autoComplete="given-name"
                          className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-gray-100 sm:text-sm sm:leading-6"
                        />
                      </div>

                      <div className="sm:col-span-3">
                        <label
                          htmlFor="status"
                          className="block text-sm font-medium leading-6 text-gray-900"
                        >
                          Status
                        </label>
                        <Listbox
                          value={toDo.toDoType}
                          onChange={(e) => dropDownHandler(e)}
                        >
                          <div className="relative">
                            <Listbox.Button className="relative w-full cursor-default rounded-lg bg-white py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white/75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                              <span className="block truncate">
                                {toDo.toDoType?.name}
                              </span>
                              <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                                <ChevronUpDownIcon
                                  className="h-5 w-5 text-gray-400"
                                  aria-hidden="true"
                                />
                              </span>
                            </Listbox.Button>
                            <Transition
                              as={Fragment}
                              leave="transition ease-in duration-100"
                              leaveFrom="opacity-100"
                              leaveTo="opacity-0"
                            >
                              <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black/5 focus:outline-none sm:text-sm">
                                {toDoTypes.value.map((toDoType) => (
                                  <Listbox.Option
                                    key={toDoType.id + "-create"}
                                    className={({ active }) =>
                                      `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                        active
                                          ? "bg-gray-100 text-gray-900"
                                          : "text-gray-900"
                                      }`
                                    }
                                    value={toDoType}
                                  >
                                    {({ selected }) => (
                                      <>
                                        <span
                                          className={`block truncate ${
                                            selected
                                              ? "font-medium"
                                              : "font-normal"
                                          }`}
                                        >
                                          {toDoType.name}
                                        </span>
                                        {selected ? (
                                          <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-gray-900">
                                            <CheckIcon
                                              className="h-5 w-5"
                                              aria-hidden="true"
                                            />
                                          </span>
                                        ) : null}
                                      </>
                                    )}
                                  </Listbox.Option>
                                ))}
                              </Listbox.Options>
                            </Transition>
                          </div>
                        </Listbox>
                      </div>
                      <div className="sm:col-span-6">
                        <label
                          htmlFor="description"
                          className="block text-sm font-medium leading-6 text-gray-900"
                        >
                          Description
                        </label>
                        <textarea
                          id="description"
                          name="description"
                          onChange={formHandler}
                          value={toDo.description}
                          rows={4}
                          className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-gray-100 sm:text-sm sm:leading-6"
                          placeholder="Description"
                        ></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
                  <button
                    type="submit"
                    className="inline-flex w-full justify-center rounded-md bg-blue-500 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-blue-800 sm:ml-3 sm:w-auto"
                  >
                    {initialTodo.value.id ? "Update" : "Create"}
                  </button>
                  <button
                    type="button"
                    className="mt-3 inline-flex w-full justify-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-200 sm:mt-0 sm:w-auto"
                    onClick={() => setModalOpen(false)}
                  >
                    Cancel
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default ModalToDo;
