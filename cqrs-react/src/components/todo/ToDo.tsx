import Accordion from "@components/todo/Accordion";
import { toDoTypes } from "@signals/ToDoList";
import { PlusIcon } from "@heroicons/react/20/solid";
import ModalToDo from "@components/todo/ModalToDo";
import { open, setModalOpen } from "@signals/CreateModalToDo";

const ToDo = () => {
  return (
    <div className="container ">
      {toDoTypes.value.map(({ id }) => {
        return <Accordion key={id + "-section"} typeId={id}></Accordion>;
      })}

      {open.value && <ModalToDo></ModalToDo>}
      <div className="group fixed bottom-0 right-0 flex  h-24 w-24 items-end justify-end p-2 ">
        <div className="delay-50 absolute z-50 flex cursor-pointer items-center justify-center rounded-full bg-gradient-to-b from-gray-800 to-gray-500 text-white shadow-xl transition duration-150 ease-in-out hover:scale-110">
          <PlusIcon
            className="m-3 h-6 w-6 "
            onClick={() => setModalOpen(true)}
          ></PlusIcon>
          {open.value}
        </div>
      </div>
    </div>
  );
};

export default ToDo;
