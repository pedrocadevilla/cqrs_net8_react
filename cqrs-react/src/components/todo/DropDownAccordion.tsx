import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon, TrashIcon } from "@heroicons/react/20/solid";
import { ToDoType } from "@interfaces/ToDoType";
import { classNames } from "@services/ClassName";
import { TypeId } from "@helpers/types/TypeId";
import { setToDos, toDos } from "@signals/ToDoList";
import { deleteTodo, updateToDo } from "@services/ToDos";

const DropDownAccordion = ({
  toDoTypes,
  toDoId,
}: {
  toDoTypes: ToDoType[];
  toDoId: TypeId;
}) => {
  const handleStatusChange = async (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>,
    typeId: TypeId,
  ) => {
    event?.preventDefault();
    const draftToDos = [...toDos.value];
    const todoIndex = draftToDos.findIndex((x) => x.id === toDoId);
    draftToDos[todoIndex].toDoType = toDoTypes.find((x) => x.id === typeId)!;
    const update: TypeId | null = await updateToDo(draftToDos[todoIndex]);
    if (update !== null) setToDos(draftToDos);
  };

  const deleteToDoAction = async (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
  ) => {
    event?.preventDefault();
    const deleted: TypeId | null = await deleteTodo(toDoId);
    if (deleted !== null) {
      const draftToDos = [...toDos.value];
      const todoIndex = draftToDos.findIndex((x) => x.id === toDoId);
      draftToDos.splice(todoIndex, 1);
      setToDos(draftToDos);
    }
  };

  return (
    <Menu as="div" className="relative flex flex-initial">
      <div
        className="m-auto inline-flex cursor-pointer pe-3 "
        onClick={(e) => deleteToDoAction(e)}
      >
        <TrashIcon className="h-6 w-6 text-red-500 hover:text-gray-500" />
      </div>
      <Menu.Button className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50">
        <span className="inline-flex justify-start">Status</span>
        <ChevronDownIcon
          className="-mr-1 h-5 w-5 text-gray-400"
          aria-hidden="true"
        />
      </Menu.Button>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
          {toDoTypes.map(({ id, name }) => {
            return (
              <div className="py-1" key={id + "-status"}>
                <Menu.Item>
                  {({ active }) => (
                    <span
                      onClick={(e) => handleStatusChange(e, id)}
                      className={classNames(
                        active
                          ? "grid grid-cols-2 grid-rows-1 bg-gray-100 text-gray-900"
                          : "text-gray-700",
                        "grid grid-cols-2 grid-rows-1 px-4 py-2 text-sm",
                      )}
                    >
                      <span className="justify-center' flex items-center">
                        {name}
                      </span>
                    </span>
                  )}
                </Menu.Item>
              </div>
            );
          })}
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default DropDownAccordion;
