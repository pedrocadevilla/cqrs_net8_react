import { Fragment, useState } from "react";
import { Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon, ChevronUpIcon } from "@heroicons/react/20/solid";
import { ToDos } from "@interfaces/ToDos";
import { ToDoType } from "@interfaces/ToDoType";
import { classNames } from "@services/ClassName";
import DropDownAccordion from "@components/todo/DropDownAccordion";
import { toDoTypes, toDos } from "@signals/ToDoList";
import { TypeId } from "@helpers/types/TypeId";
import { getToDoById } from "@services/ToDos";
import { setModalOpen } from "@signals/CreateModalToDo";
import { setInitialTodo } from "@signals/InitialToDo";

const Accordion = ({ typeId }: { typeId: TypeId }) => {
  const filteredToDos: ToDos[] =
    toDos.value.length > 0
      ? toDos.value
          .filter((x) => x.toDoType.id === typeId)
          .sort((a, b) => {
            return a.title.localeCompare(b.title);
          })
      : [];
  const [open, setOpen] = useState(true);
  const openTask = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event?.preventDefault();
    setOpen(!open);
  };

  const getTodoModal = async (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>,
    toDoId: TypeId,
  ) => {
    event?.preventDefault();
    const selectedTodo: ToDos | null = await getToDoById(toDoId);
    if (selectedTodo !== null) {
      setInitialTodo(selectedTodo);
      setModalOpen(true);
    }
  };

  const availableStatus: ToDoType[] = toDoTypes.value.filter(
    (x) => x.id !== typeId,
  );

  return (
    <Menu as="div" className="relative inline-block w-full p-4 text-left">
      <Menu.Button
        onClick={(e) => openTask(e)}
        className="grid w-full grid-cols-2 grid-rows-1 gap-x-1.5 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-400 hover:bg-gray-50"
      >
        <span className="inline-flex justify-start">
          {toDoTypes.value.find((x) => x.id === typeId)!.name}
        </span>
        <div className="inline-flex justify-end">
          {open ? (
            <ChevronUpIcon
              className="-mr-1 h-5 w-5 text-gray-400"
              aria-hidden="true"
            />
          ) : (
            <ChevronDownIcon
              className="-mr-1 h-5 w-5 text-gray-400"
              aria-hidden="true"
            />
          )}
        </div>
      </Menu.Button>

      <Transition
        show={open}
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="z-10 mt-2 w-full origin-top-right rounded-md bg-white shadow-lg ring-1 ring-gray-400 ring-opacity-5 focus:outline-none">
          {filteredToDos.length > 0 ? (
            filteredToDos.map(({ id, title }) => {
              return (
                <div className="my-1" key={id + "-toDo"}>
                  <Menu.Item>
                    {({ active }) => (
                      <span
                        onClick={(e) => getTodoModal(e, id)}
                        className={classNames(
                          active
                            ? "grid grid-cols-2 grid-rows-1 bg-gray-300 text-gray-900"
                            : "text-gray-700",
                          "grid grid-cols-2 grid-rows-1 rounded-md px-4 py-2 text-sm",
                        )}
                      >
                        <span className="justify-center' flex items-center">
                          {title}
                        </span>
                        <div className="flex justify-end">
                          <DropDownAccordion
                            toDoTypes={availableStatus}
                            toDoId={id}
                          ></DropDownAccordion>
                        </div>
                      </span>
                    )}
                  </Menu.Item>
                </div>
              );
            })
          ) : (
            <div className="my-1">
              <Menu.Item>
                {({ active }) => (
                  <span
                    className={classNames(
                      active
                        ? "grid grid-cols-2 grid-rows-1 bg-gray-100 text-gray-900"
                        : "text-gray-700",
                      "grid grid-cols-2 grid-rows-1 rounded-md px-4 py-2 text-sm",
                    )}
                  >
                    <span className="justify-center' flex items-center">
                      No to dos available to show
                    </span>
                  </span>
                )}
              </Menu.Item>
            </div>
          )}
        </Menu.Items>
      </Transition>
    </Menu>
  );
};
export default Accordion;
