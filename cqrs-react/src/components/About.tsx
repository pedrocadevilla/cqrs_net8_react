import { GlobeAltIcon } from "@heroicons/react/24/solid";
import { BookOpenIcon } from "@heroicons/react/24/solid";
import { PaintBrushIcon } from "@heroicons/react/24/solid";
import { CircleStackIcon } from "@heroicons/react/24/solid";
import { CloudIcon } from "@heroicons/react/24/solid";
import { PhoneIcon } from "@heroicons/react/24/solid";
import { EnvelopeIcon } from "@heroicons/react/24/solid";
import { LanguageIcon } from "@heroicons/react/24/solid";

const About = () => {
  return (
    <div className="container mx-auto mt-5 max-w-5xl">
      <div className="grid grid-cols-3 grid-rows-1 gap-4">
        <div className="col-span-2 space-y-3 p-4">
          <h1>Pedro Cadevilla</h1>
          <h3>Technical Lead (1 year) & Full Stack Developer (7 years) </h3>
          <span className="flex items-center">
            <LanguageIcon className="mr-2 h-5 w-5 text-gray-500" />
            Bilingual (Spanish/English)
          </span>
          <div className="grid grid-cols-4 grid-rows-1 gap-2 max-md:grid-cols-1">
            <div>
              <span className="flex items-center font-semibold">
                <BookOpenIcon className="mr-2 h-5 w-5 text-gray-500" /> Backend
                Skills
              </span>
              <ul>
                <li>.Net</li>
                <li>Python</li>
                <li>Node</li>
              </ul>
            </div>
            <div>
              <span className="flex items-center font-semibold">
                <PaintBrushIcon className="mr-2 h-5 w-5 text-gray-500" />{" "}
                FrontEnd Skills
              </span>
              <ul>
                <li>Angular</li>
                <li>React</li>
                <li>Vue</li>
              </ul>
            </div>
            <div>
              <span className="flex items-center font-semibold">
                <CircleStackIcon className="mr-2 h-5 w-5 text-gray-500" />{" "}
                Database Skills
              </span>
              <ul>
                <li>Data Modeling</li>
                <li>SQL and NoSQL DB</li>
                <li>Entity Framework</li>
              </ul>
            </div>
            <div>
              <span className="flex items-center font-semibold">
                <CloudIcon className="mr-2 h-5 w-5 text-gray-500" /> DevOps
                Skills
              </span>
              <ul>
                <li>AWS</li>
                <li>CI/CD</li>
                <li>Docker</li>
              </ul>
            </div>
          </div>
          <div className="flex items-center space-x-2">
            <span className="flex items-center font-semibold">
              <EnvelopeIcon className="mr-1 h-5 w-5 text-gray-500" />
              pedro.cadevilla@gmail.com
            </span>
            <span className="flex items-center font-semibold">
              <PhoneIcon className="mr-2 h-5 w-5 text-gray-500" /> +584125298180
            </span>
          </div>
          <span className="flex items-center font-semibold">
            <GlobeAltIcon className="mr-1 h-5 w-5 text-gray-500" /> Tachira,
            Venezuela
          </span>
        </div>
        <div className="flex items-center justify-center">
          <img src="profile.png" alt="Profile Picture" />
        </div>
      </div>
      <div className="grid grid-flow-row space-y-3 p-4">
        <h2>Career Path</h2>
        <div>
          <h4>Creditos Directos S.A. (3 years)</h4>
          <span>
            My work here consisted in developing a mobile and web app for a bank
            company located in Uruguay where we needed to be able to integrate
            the mobile platform with some third party api to perform payments,
            transfers and other transactions using the mobile app but at the
            same time they wanted to have reports of the activities performed by
            the users so the web app consisted in having a report generator with
            graphs and statistics so the employers could evaluate more
            accurately the users for the moment of a loan evaluation in this
            project I was 1 year working as a JR BackEnd developer and the
            second year as a JR FullStack Developer.
          </span>
        </div>
        <div>
          <h4>World Cloud Services (1 year)</h4>
          <span>
            In this job I was asked to validate current code and fix
            vulnerabilities in order to pass a series of validation their site
            needed to pass in order for the application to be cleared for
            production deployment after some iso approvals so we need to create
            some pipes and some guards to the all FE also needed to add some
            encrypting to the DB validate HTTPS certificates and upgrade some
            old technologies been used with known vulnerabilities in this
            project I was acting as a Sr Full-Stack Developer for 1 years.
          </span>
        </div>
        <div>
          <h4>BairesDev (3 years)</h4>
          <span>
            Currently I'm working in a project as a Project Lead of a 5 people
            team where we are in charge of maintaining and improvement of a
            platform that provides companies and users best paths to save their
            money and improve their retirement incomes besides givin a detailed
            study about the client money saving behavior in this project we have
            been integrating with third party api like twilio app for sending
            sms and mass email or like CK-Editor to create personalized HTML
            email templates we have been deploying using gitlab pipelines with
            AWS we use micro frontend and micro backend located in containers
            all EC2 (Not my choice the architecture was already in place when we
            arrive to the project) all the project is dockerize and its
            currently been upgrade to latest version of software since it was
            using angular 8 and python 2 which showed a lot of vulnerabilities
            not wanted by the client.
          </span>
        </div>
      </div>
    </div>
  );
};

export default About;
