import { TypeId } from "@helpers/types/TypeId";
import { ToDos } from "@interfaces/ToDos";

const URL: string = import.meta.env.VITE_URL;
export const getToDos = (): Promise<ToDos[]> => {
  return fetch(`${URL}/toDos`)
    .then((res) => res.json() as Promise<{ value: ToDos[] }>)
    .then((res) => {
      if (res.value) return res.value;
      return [];
    })
    .catch(() => {
      return [];
    });
};

export const updateToDo = (data: ToDos): Promise<TypeId | null> => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      toDoId: data.id,
      toDoTypeId: data.toDoType.id,
      description: data.description,
      title: data.title,
    }),
  };

  return fetch(`${URL}/toDo/update`, requestOptions)
    .then((res) => res.json() as Promise<{ value: TypeId }>)
    .then((res) => {
      return res.value;
    })
    .catch(() => {
      return null;
    });
};

export const createToDo = (data: ToDos): Promise<TypeId | null> => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: data.title,
      description: data.description,
      toDoTypeId: data.toDoType.id,
    }),
  };

  return fetch(`${URL}/toDo`, requestOptions)
    .then((res) => res.json() as Promise<{ value: TypeId }>)
    .then((res) => {
      return res.value;
    })
    .catch(() => {
      return null;
    });
};

export const deleteTodo = (id: TypeId): Promise<TypeId | null> => {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
  };

  return fetch(`${URL}/toDo/${id}`, requestOptions)
    .then((res) => res.json() as Promise<{ value: TypeId }>)
    .then((res) => {
      return res.value;
    })
    .catch(() => {
      return null;
    });
};

export const getToDoById = (id: TypeId): Promise<ToDos | null> => {
  return fetch(`${URL}/toDo/${id}`)
    .then((res) => res.json() as Promise<{ value: ToDos }>)
    .then((res) => {
      return res.value;
    })
    .catch(() => {
      return null;
    });
};
