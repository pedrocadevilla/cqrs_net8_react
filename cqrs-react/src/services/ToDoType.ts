import { ToDoType } from "@interfaces/ToDoType";

const URL: string = import.meta.env.VITE_URL;
export const getToDoTypes = (): Promise<ToDoType[]> => {
  return fetch(`${URL}/toDoType`)
    .then((res) => res.json() as Promise<{ value: ToDoType[] }>)
    .then((res) => {
      if (res.value) return res.value;
      return [];
    })
    .catch(() => {
      return [];
    });
};
