import { describe, it, expect, vi } from "vitest";

import { getToDoTypes } from "../services/ToDoType";
const URL: string = import.meta.env.VITE_URL;
describe("Functionality of the get types service", () => {
  it("should return a Promise that resolves to an array of ToDoType objects when the API call is successful", async () => {
    const mockResponse = {
      value: [
        { name: "Type 1", id: "1" },
        { name: "Type 2", id: "2" },
      ],
    };
    vi.spyOn(global, "fetch").mockResolvedValueOnce({
      json: vi.fn().mockResolvedValueOnce(mockResponse),
    } as unknown as Response);

    const result = await getToDoTypes();

    expect(result).toEqual(mockResponse.value);
    expect(global.fetch).toHaveBeenCalledWith(`${URL}/toDoType`);
  });

  it("should return an array of ToDoType objects with different name and id values", async () => {
    const mockResponse = {
      value: [
        { name: "Type 1", id: "1" },
        { name: "Type 2", id: "2" },
      ],
    };
    vi.spyOn(global, "fetch").mockResolvedValueOnce({
      json: vi.fn().mockResolvedValueOnce(mockResponse),
    } as unknown as Response);

    const result = await getToDoTypes();

    expect(result).toEqual(mockResponse.value);
    expect(global.fetch).toHaveBeenCalledWith(`${URL}/toDoType`);
  });

  it("should return an empty array when the API call is unsuccessful", async () => {
    vi.spyOn(global, "fetch").mockRejectedValueOnce(new Error());

    const result = await getToDoTypes();

    expect(result).toEqual([]);
    expect(global.fetch).toHaveBeenCalledWith(`${URL}/toDoType`);
  });

  it("should return an empty array when the API returns an empty array", async () => {
    const mockResponse = { value: [] };
    vi.spyOn(global, "fetch").mockResolvedValueOnce({
      json: vi.fn().mockResolvedValueOnce(mockResponse),
    } as unknown as Response);

    const result = await getToDoTypes();

    expect(result).toEqual([]);
    expect(global.fetch).toHaveBeenCalledWith(`${URL}/toDoType`);
  });

  it("should return an array of ToDoType objects with different name and id values", async () => {
    const mockResponse = {
      value: [
        { name: "Type 1", id: "1" },
        { name: "Type 2", id: "2" },
      ],
    };
    vi.spyOn(global, "fetch").mockResolvedValueOnce({
      json: vi.fn().mockResolvedValueOnce(mockResponse),
    } as unknown as Response);

    const result = await getToDoTypes();

    expect(result).toEqual(mockResponse.value);
    expect(global.fetch).toHaveBeenCalledWith(`${URL}/toDoType`);
  });
});
