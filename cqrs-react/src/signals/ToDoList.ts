import { ToDoType } from "@interfaces/ToDoType";
import { ToDos } from "@interfaces/ToDos";
import { signal } from "@preact/signals-react";
import { getToDoTypes } from "@services/ToDoType";
import { getToDos } from "@services/ToDos";

const initialToDos: ToDos[] = await getToDos();

export const toDos = signal(initialToDos);

export const setToDos = (value: ToDos[] = []) => {
    toDos.value = value;
}

const toDoTypeList: ToDoType[] = await getToDoTypes();

export const toDoTypes = signal(toDoTypeList);
