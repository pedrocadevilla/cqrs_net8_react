import { signal } from "@preact/signals-react";
import { setInitialTodo } from "./InitialToDo";

export const open = signal(false);

export const setModalOpen = (value: boolean) => {
  if (!value) {
    setInitialTodo();
  }
  open.value = value;
};
