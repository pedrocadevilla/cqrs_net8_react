import { toDoTypes } from "./ToDoList";
import { ToDos } from "@interfaces/ToDos";
import { signal } from "@preact/signals-react";

const draftToDo: Partial<ToDos> = {
  toDoType: toDoTypes.value[0],
  title: "",
};
export const initialTodo = signal(draftToDo);

export const setInitialTodo = (toDo?: Partial<ToDos> | ToDos) => {
    if(toDo) {
        initialTodo.value = toDo;
    }else{
        initialTodo.value = draftToDo;
    }
  
};
