const config = {
  trailingComma: "es5",
  tabWidth: 4,
  semi: false,
  singleQuote: true,
};

export default config.exports = {
  plugins: ["prettier-plugin-tailwindcss"],
};
