﻿using Domain.ToDoTypes;

namespace Domain.ToDos;

public class ToDo
{
    public ToDo(ToDoId id, string title, string description, ToDoType toDoType)
    {
        Id = id;
        Name = id.Value.ToString().Split("-")[0];
        Description = description;
        Title = title;
        ToDoType = toDoType;
        ToDoTypeId = toDoType.Id;
        CreatedAt = DateTime.Now;
        UpdatedAt = DateTime.Now;
    }

    private ToDo()
    {
    }

    public ToDoId Id { get; private set; }
    public string Name { get; private set; } = string.Empty;
    public string Title { get; private set; } = string.Empty;
    public string Description { get; private set; } = string.Empty;
    public ToDoTypeId ToDoTypeId { get; private set; }
    public ToDoType ToDoType {get; private set; }
    public DateTime CreatedAt { get; private set; }
    public DateTime UpdatedAt { get; private set; }

    public void Update(string title, string description, ToDoType toDoType)
    {
        Description = description; 
        Title = title;
        ToDoType = toDoType;
        ToDoTypeId = toDoType.Id;
        UpdatedAt = DateTime.Now;
    }
}
