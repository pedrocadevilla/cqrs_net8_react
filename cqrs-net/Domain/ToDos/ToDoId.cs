﻿namespace Domain.ToDos;

public record ToDoId(Guid Value);