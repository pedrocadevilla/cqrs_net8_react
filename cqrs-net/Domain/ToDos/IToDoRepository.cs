﻿namespace Domain.ToDos;

public interface IToDoRepository
{
    Task<ToDo?> GetByIdAsync(ToDoId id, CancellationToken cancellationToken);

    Task<List<ToDo>> GetAsync(CancellationToken cancellationToken);

    void Add(ToDo task);

    void Update(ToDo task);

    void Remove(ToDo task);
}