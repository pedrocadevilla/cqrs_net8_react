﻿using Domain.ToDos;

namespace Domain.ToDoTypes;

public interface IToDoTypeRepository
{
    void Add(ToDoType task);

    Task<List<ToDoType>> GetAsync(CancellationToken cancellationToken);

    Task<ToDoType?> GetByIdAsync(ToDoTypeId id, CancellationToken cancellationToken);
}
