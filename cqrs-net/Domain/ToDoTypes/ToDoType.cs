﻿using Domain.ToDos;

namespace Domain.ToDoTypes;

public class ToDoType
{
    public ToDoType(ToDoTypeId id, string name, int priority)
    {
        Id = id;
        Name = name;
        Priority = priority;
    }

    private ToDoType()
    {
    }
    public ToDoTypeId Id { get; private set; }

    public string Name { get; private set; } = string.Empty;

    public int Priority { get; private set; }

    public ICollection<ToDo> Todo { get; private set; }
}