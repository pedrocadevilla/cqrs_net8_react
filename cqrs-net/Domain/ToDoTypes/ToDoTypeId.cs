﻿namespace Domain.ToDoTypes;

public record ToDoTypeId(Guid Value);