﻿using Domain.Shared;

namespace Domain.Errors;
public static class DomainErrors
{
    public static class FirstName
    {
        public static readonly Error Empty = new(
            "FirstName.Empty",
            "First name is empty");

        public static readonly Error TooLong = new(
            "LastName.TooLong",
            "FirstName name is too long");
    }

    public static class LastName
    {
        public static readonly Error Empty = new(
            "LastName.Empty",
            "Last name is empty");

        public static readonly Error TooLong = new(
            "LastName.TooLong",
            "Last name is too long");
    }

    public static class ToDos
    {
        public static readonly Func<Guid, Error> NotFound = id => new Error(
            "Permission.NotFound",
            $"The toDo with the identifier {id} was not found.");

        public static readonly Func<Guid, Error> TypeNotFound = id => new Error(
            "Permission.TypeNotFound",
            $"The toDo type with the identifier {id} was not found.");

        public static readonly Error NoData = new(
            "Permission.NoData",
            "There are no toDos on the database");
    }

    public static class ToDosType
    {
        public static readonly Func<Guid, Error> NotFound = id => new Error(
            "PermissionType.NotFound",
            $"The toDo type with the identifier {id} was not found.");
    }
}
