﻿using Application.Abstractions.Messaging;
using Application.Data;
using Domain.Errors;
using Domain.Shared;
using Domain.ToDos;
using Domain.ToDoTypes;

namespace Application.ToDos.Update;

internal sealed class UpdateToDoCommandHandler : ICommandHandler<UpdateToDoCommand, Guid>
{
    private readonly IToDoRepository _toDoRepository;
    private readonly IToDoTypeRepository _toDoTypeRepository;
    private readonly IUnitOfWork _unitOfWork;

    public UpdateToDoCommandHandler(IToDoRepository toDoRepository, IToDoTypeRepository toDoTypeRepository, IUnitOfWork unitOfWork)
    {
        _toDoRepository = toDoRepository;
        _toDoTypeRepository = toDoTypeRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<Guid>> Handle(UpdateToDoCommand request, CancellationToken cancellationToken)
    {
        var toDo = await _toDoRepository.GetByIdAsync(new ToDoId(request.ToDoId), cancellationToken);

        if (toDo is null)
        {
            return Result.Failure<Guid>(DomainErrors.ToDos.NotFound(new ToDoId(request.ToDoId).Value));
        }

        var toDoType = await _toDoTypeRepository.GetByIdAsync(new ToDoTypeId(request.ToDoTypeId), cancellationToken);

        if (toDoType is null)
        {
            return Result.Failure<Guid>(DomainErrors.ToDosType.NotFound(toDo.ToDoType.Id.Value));
        }

        toDo.Update(request.Title, request.Description,  toDoType);

        _toDoRepository.Update(toDo);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return request.ToDoId;
    }
}
