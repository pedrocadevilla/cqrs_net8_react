﻿using Application.Abstractions.Messaging;
using Domain.ToDos;
using Domain.ToDoTypes;

namespace Application.ToDos.Update;

public record UpdateToDoCommand(
    Guid ToDoId,
    string Title,
    string Description,
    Guid ToDoTypeId) : ICommand<Guid>;
