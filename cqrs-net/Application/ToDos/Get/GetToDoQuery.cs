﻿using Application.Abstractions.Messaging;
using Application.ToDos.Models;
using Domain.ToDos;

namespace Application.ToDos.Get;

public record GetToDoQuery(ToDoId ToDoId): IQuery<ToDoResponse>;