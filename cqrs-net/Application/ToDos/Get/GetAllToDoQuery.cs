﻿using Application.Abstractions.Messaging;
using Application.ToDos.Models;

namespace Application.ToDos.Get;

public record GetAllToDoQuery() : IQuery<List<ToDoResponse>>;