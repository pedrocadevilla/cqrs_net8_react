﻿using Application.Abstractions.Messaging;
using Application.ToDos.Models;
using Application.ToDoTypes.Models;
using Domain.Errors;
using Domain.Shared;
using Domain.ToDos;
using Domain.ToDoTypes;

namespace Application.ToDos.Get;

internal sealed class GetToDoQueryHandler : IQueryHandler<GetToDoQuery,ToDoResponse>
{
    private readonly IToDoTypeRepository _toDoTypeRepository;
    private readonly IToDoRepository _toDoRepository;

    public GetToDoQueryHandler(IToDoTypeRepository toDoTypeRepository, IToDoRepository toDoRepository)
    {
        _toDoTypeRepository = toDoTypeRepository;
        _toDoRepository = toDoRepository;
    }

    public async Task<Result<ToDoResponse>> Handle(GetToDoQuery request, CancellationToken cancellationToken)
    {
        var toDo = await _toDoRepository.GetByIdAsync(request.ToDoId, cancellationToken);

        if (toDo is null)
        {
            return Result.Failure<ToDoResponse>(DomainErrors.ToDos.NotFound(request.ToDoId.Value));
        }

        return new ToDoResponse(
               toDo.Id.Value, toDo.Name, toDo.Description, toDo.Title ,new ToDoTypeResponse(toDo.ToDoType.Id.Value, toDo.ToDoType.Name, toDo.ToDoType.Priority));
    }
}
