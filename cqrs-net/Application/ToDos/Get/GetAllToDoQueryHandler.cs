﻿using Application.Abstractions.Messaging;
using Application.Data;
using Application.ToDos.Models;
using Application.ToDoTypes.Models;
using Domain.Errors;
using Domain.Shared;
using Domain.ToDos;

namespace Application.ToDos.Get;

internal sealed class GetAllToDoQueryHandler : IQueryHandler<GetAllToDoQuery, List<ToDoResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IToDoRepository _toDoRepository;

    public GetAllToDoQueryHandler(IApplicationDbContext context, IToDoRepository toDoRepository)
    {
        _context = context;
        _toDoRepository = toDoRepository;
    }

    public async Task<Result<List<ToDoResponse>>> Handle(GetAllToDoQuery request, CancellationToken cancellationToken)
    {

        var toDos = await _toDoRepository.GetAsync(cancellationToken);

        if (toDos.Count == 0)
        {
            return Result.Failure<List<ToDoResponse>>(DomainErrors.ToDos.NoData);
        }

        var toDoResponse = toDos.Select(x => new ToDoResponse(x.Id.Value, x.Name, x.Description, x.Title, new ToDoTypeResponse(x.ToDoType.Id.Value, x.ToDoType.Name, x.ToDoType.Priority))).ToList();
            

        return toDoResponse;
    }
}
