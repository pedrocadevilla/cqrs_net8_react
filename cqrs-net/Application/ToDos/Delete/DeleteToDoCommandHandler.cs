﻿using Application.Abstractions.Messaging;
using Application.Data;
using Domain.Errors;
using Domain.Shared;
using Domain.ToDos;

namespace Application.ToDos.Delete;

internal sealed class DeleteToDoCommandHandler : ICommandHandler<DeleteToDoCommand,Guid>
{
    private readonly IToDoRepository _productRepository;
    private readonly IUnitOfWork _unitOfWork;

    public DeleteToDoCommandHandler(IToDoRepository productRepository, IUnitOfWork unitOfWork)
    {
        _productRepository = productRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<Guid>> Handle(DeleteToDoCommand request, CancellationToken cancellationToken)
    {
        var toDo = await _productRepository.GetByIdAsync(request.PermissionId, cancellationToken);

        if (toDo is null)
        {
            return Result.Failure<Guid>(DomainErrors.ToDos.NotFound(request.PermissionId.Value));
        }

        _productRepository.Remove(toDo);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return request.PermissionId.Value;
    }
}
