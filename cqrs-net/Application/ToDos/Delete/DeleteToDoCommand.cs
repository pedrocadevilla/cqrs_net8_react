﻿using Application.Abstractions.Messaging;
using Domain.ToDos;

namespace Application.ToDos.Delete;

public record DeleteToDoCommand(ToDoId PermissionId) : ICommand<Guid>;
