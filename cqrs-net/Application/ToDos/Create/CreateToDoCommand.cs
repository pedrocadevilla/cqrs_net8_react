﻿using Application.Abstractions.Messaging;
using Domain.ToDoTypes;

namespace Application.ToDos.Create;

public sealed record CreateToDoCommand(
    string Title,
    string Description,
    Guid TodoTypeId) : ICommand<Guid>;
