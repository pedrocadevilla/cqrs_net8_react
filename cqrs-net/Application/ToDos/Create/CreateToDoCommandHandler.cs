﻿using Application.Abstractions.Messaging;
using Application.Data;
using Application.ToDoTypes.Create;
using Domain.Errors;
using Domain.Shared;
using Domain.ToDos;
using Domain.ToDoTypes;

namespace Application.ToDos.Create;

internal class CreateToDoCommandHandler : ICommandHandler<CreateToDoCommand, Guid>
{
    private readonly IToDoRepository _toDoRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IToDoTypeRepository _toDoTypeRepository;

    public CreateToDoCommandHandler(
        IToDoRepository toDoRepository,
        IUnitOfWork unitOfWork,
        IToDoTypeRepository toDoTypeRepository)
    {
        _toDoRepository = toDoRepository;
        _unitOfWork = unitOfWork;
        _toDoTypeRepository = toDoTypeRepository;
    }

    public async Task<Result<Guid>> Handle(CreateToDoCommand request, CancellationToken cancellationToken)
    {
        var toDoType = await _toDoTypeRepository.GetByIdAsync(new ToDoTypeId(request.TodoTypeId), cancellationToken);

        if (toDoType is null)
        {
            return Result.Failure<Guid>(DomainErrors.ToDos.TypeNotFound(request.TodoTypeId));
        }

        var toDo = new ToDo(
            new ToDoId(Guid.NewGuid()),
            request.Title,
            request.Description,
            toDoType);

        _toDoRepository.Add(toDo);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return toDo.Id.Value;
    }
}
