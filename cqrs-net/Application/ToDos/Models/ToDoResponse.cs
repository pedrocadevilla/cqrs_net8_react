﻿using Application.ToDoTypes.Models;
using Domain.ToDoTypes;

namespace Application.ToDos.Models;

public record ToDoResponse(
    Guid Id,
    string Name,
    string Description,
    string Title,
    ToDoTypeResponse ToDoType
    );

