﻿namespace Application.ToDoTypes.Models;

public record ToDoTypeResponse(
    Guid Id,
    string Name,
    int Priority);
