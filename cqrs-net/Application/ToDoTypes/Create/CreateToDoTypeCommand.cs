﻿using Application.Abstractions.Messaging;

namespace Application.ToDoTypes.Create;

public sealed record CreateToDoTypeCommand(
    string Title, int Priority) : ICommand<Guid>;
