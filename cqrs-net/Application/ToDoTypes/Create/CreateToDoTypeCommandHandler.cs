﻿using Application.Abstractions.Messaging;
using Application.Data;
using Domain.Errors;
using Domain.Shared;
using Domain.ToDos;
using Domain.ToDoTypes;

namespace Application.ToDoTypes.Create;

internal class CreateToDoTypeCommandHandler : ICommandHandler<CreateToDoTypeCommand, Guid>
{
    private readonly IToDoRepository _toDoRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IToDoTypeRepository _toDoTypeRepository;

    public CreateToDoTypeCommandHandler(
        IToDoRepository toDoRepository,
        IUnitOfWork unitOfWork,
        IToDoTypeRepository toDoTypeRepository)
    {
        _toDoRepository = toDoRepository;
        _unitOfWork = unitOfWork;
        _toDoTypeRepository = toDoTypeRepository;
    }

    public async Task<Result<Guid>> Handle(CreateToDoTypeCommand request, CancellationToken cancellationToken)
    {
        /*var toDoType = await _toDoTypeRepository.GetB(request.TodoType.Id, cancellationToken);

        if (toDoType is null)
        {
            return Result.Failure<Guid>(DomainErrors.ToDos.TypeNotFound(request.TodoType.Id.Value));
        }*/

        var toDoType = new ToDoType(
            new ToDoTypeId(Guid.NewGuid()),
            request.Title, request.Priority);

        _toDoTypeRepository.Add(toDoType);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return toDoType.Id.Value;
    }
}
