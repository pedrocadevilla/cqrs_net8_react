﻿using Application.Abstractions.Messaging;
using Application.Data;
using Application.ToDoTypes.Models;
using Domain.Errors;
using Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace Application.ToDoTypes.Get;

internal sealed class GetToDoTypeQueryHandler : IQueryHandler<GetToDoTypeQuery, ToDoTypeResponse>
{
    private readonly IApplicationDbContext _context;

    public GetToDoTypeQueryHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Result<ToDoTypeResponse>> Handle(GetToDoTypeQuery request, CancellationToken cancellationToken)
    {
        var taskType = await _context
            .ToDoTypes
            .Select(pt => new ToDoTypeResponse(
                pt.Id.Value,
                pt.Name,
                pt.Priority
                ))
            .FirstOrDefaultAsync(cancellationToken);

        if (taskType is null)
        {
            return Result.Failure<ToDoTypeResponse>(DomainErrors.ToDosType.NotFound(request.todoTypeId.Value));
        }

        return taskType;
    }
}
