﻿using Application.Abstractions.Messaging;
using Application.Data;
using Application.ToDoTypes.Models;
using Domain.Errors;
using Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace Application.ToDoTypes.Get;

internal sealed class GetAllToDoTypeQueryHandler : IQueryHandler<GetAllToDoTypeQuery, List<ToDoTypeResponse>>
{
    private readonly IApplicationDbContext _context;

    public GetAllToDoTypeQueryHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Result<List<ToDoTypeResponse>>> Handle(GetAllToDoTypeQuery request, CancellationToken cancellationToken)
    {
        var taskTypes = await _context
            .ToDoTypes
            .Select(tt => new ToDoTypeResponse(
                tt.Id.Value,
                tt.Name,
                tt.Priority
                ))
            .ToListAsync(cancellationToken);

        if (taskTypes is null)
        {
            return Result.Failure<List<ToDoTypeResponse>>(DomainErrors.ToDos.NoData);
        }

        return taskTypes.OrderBy(x => x.Priority).ToList();
    }
}
