﻿using Application.Abstractions.Messaging;
using Application.ToDoTypes.Models;
using Domain.ToDoTypes;

namespace Application.ToDoTypes.Get;

public record GetToDoTypeQuery(ToDoTypeId todoTypeId) : IQuery<ToDoTypeResponse>;