﻿using Application.Abstractions.Messaging;
using Application.ToDoTypes.Models;

namespace Application.ToDoTypes.Get;

public record GetAllToDoTypeQuery() : IQuery<List<ToDoTypeResponse>>;