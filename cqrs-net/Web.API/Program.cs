using Application;
using Carter;
using Persistence;
using Web.API.Extensions;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddPersistence(builder.Configuration);
builder.Services.AddApplication();
builder.Services.AddCarter();

builder.Services.AddCors(options =>
{
    options.AddPolicy(builder.Configuration.GetSection("CORS").GetValue<string>("PolicyName")!,
        corsBuilder =>
        {
        corsBuilder.WithOrigins(builder.Configuration.GetSection("CORS:AllowedHosts").GetChildren().Select(x => x.Value).ToArray()!)
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.ApplyMigrations();

app.UseCors(builder.Configuration.GetSection("CORS").GetValue<string>("PolicyName")!);

app.UseHttpsRedirection();

app.UseRateLimiter();

app.MapCarter();

app.Run();
