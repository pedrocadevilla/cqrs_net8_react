﻿using Application.ToDos.Delete;
using Application.ToDos.Get;
using Application.ToDos.Update;
using Carter;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Domain.ToDos;
using Application.ToDos.Create;
using Application.ToDos.Models;
using Domain.Shared;

namespace Web.API.Endpoints;

public class ToDo : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapPost("toDo", async (CreateToDoCommand command, ISender sender) =>
        {
            try
            {
                return Results.Ok(await sender.Send(command));
            }
            catch (Exception e)
            {
                return Results.BadRequest(e);
            }      
        });

        app.MapGet("toDos", async (ISender sender) =>
        {
            try
            {
                Result<List<ToDoResponse>> response = await sender.Send(new GetAllToDoQuery());
                return response.IsSuccess ? Results.Ok(response) : Results.NoContent();
            }
            catch (Exception e)
            {   
                return Results.NotFound(e);
            }
        });

        app.MapGet("toDo/{id:guid}", async (Guid id, ISender sender) =>
        {
            try
            {
                return Results.Ok(await sender.Send(new GetToDoQuery(new ToDoId(id))));
            }
            catch (Exception e) { return Results.NotFound(e); }
        });

        app.MapPut("toDo/update", async ([FromBody] UpdateToDoCommand request, ISender sender) =>
        {
            try
            {
                return Results.Ok(await sender.Send(request));
            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });

        app.MapDelete("toDo/{id:guid}", async (Guid id, ISender sender) =>
        {
            try
            {
                return Results.Ok(await sender.Send(new DeleteToDoCommand(new ToDoId(id))));
            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });
    }
}
