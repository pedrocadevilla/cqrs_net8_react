﻿using Application.ToDoTypes.Create;
using Application.ToDoTypes.Get;
using Carter;
using Domain.ToDoTypes;
using MediatR;


namespace Web.API.Endpoints;

public class PermissionTypes : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapPost("toDoType", async (CreateToDoTypeCommand command, ISender sender) =>
        {
            await sender.Send(command);

            return Results.Ok();
        });

        app.MapGet("toDoType", async (ISender sender) =>
        {
            try
            {
                var result = await sender.Send(new GetAllToDoTypeQuery());
                if (result.IsSuccess)
                {
                    return Results.Ok(result);

                }
                else
                {
                    return Results.BadRequest(result);
                }
                
            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });

        app.MapGet("toDoType/{id:guid}", async (Guid id, ISender sender) =>
        {
            try
            {
                var result = await sender.Send(new GetToDoTypeQuery(new ToDoTypeId(id)));
                if (result.IsSuccess)
                {
                    return Results.Ok(result);

                }
                else
                {
                    return Results.BadRequest(result);
                }

            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });
    }
}
