﻿using FluentAssertions;
using Moq;
using Application.Data;
using Domain.Shared;
using Domain.Errors;
using Domain.ToDos;
using Domain.ToDoTypes;
using Application.ToDos.Create;

namespace Tests.Members.Commands;

public class CreateTodosCommandHandlerTests
{
    private readonly Mock<IToDoRepository> _toDoRepositoryMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IToDoTypeRepository> _toDoTypeMock;

    public CreateTodosCommandHandlerTests()
    {
        _toDoRepositoryMock = new();
        _unitOfWorkMock = new();
        _toDoTypeMock = new();
    }

    [Fact]
    public async System.Threading.Tasks.Task Handle_Should_ReturnFailureResult_WhenPermissionTypeGuidNotFound()
    {
        // Arrange
        var typeId = Guid.NewGuid();
        ToDoType? toDo = null;
        var command = new CreateToDoCommand("first", "description", Guid.NewGuid());
        _toDoTypeMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<ToDoTypeId>(), 
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(toDo);

        var handler = new CreateToDoCommandHandler(
            _toDoRepositoryMock.Object,
            _unitOfWorkMock.Object,
            _toDoTypeMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.ToDos.TypeNotFound(typeId));
    }

    [Fact]
    public async Task Handle_Should_ReturnSuccessResult_WhenPermissionTypeGuidFound()
    {
        // Arrange
        var typeId = Guid.NewGuid();
        var command = new CreateToDoCommand("first", "description", Guid.NewGuid());
        ToDoType? toDoType = new ToDoType(new ToDoTypeId(typeId), "a", 1);
        _toDoTypeMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<ToDoTypeId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(toDoType);

        var handler = new CreateToDoCommandHandler(
            _toDoRepositoryMock.Object,
            _unitOfWorkMock.Object,
            _toDoTypeMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsSuccess.Should().BeTrue();
        _toDoRepositoryMock.Verify(
            x => x.Add(It.Is<ToDo>(m => m.Id == new ToDoId(result.Value))),
            Times.Once);
    }
}
