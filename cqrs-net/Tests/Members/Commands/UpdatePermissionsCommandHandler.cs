﻿using FluentAssertions;
using Moq;
using Application.Data;
using Domain.Shared;
using Domain.Errors;
using Application.ToDos.Update;
using Domain.ToDos;
using Domain.ToDoTypes;

namespace Tests.Members.Commands;

public class UpdateTodosCommandHandlerTests
{
    private readonly Mock<IToDoRepository> _toDoRepositoryMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IToDoTypeRepository> _toDoTypeMock;

    public UpdateTodosCommandHandlerTests()
    {
        _toDoRepositoryMock = new();
        _unitOfWorkMock = new();
        _toDoTypeMock = new();
    }

    [Fact]
    public async Task Handle_Should_ReturnFailureResult_WhenPermissionGuidNotFound()
    {
        // Arrange
        var toDoId = Guid.NewGuid();
        var toDoTypeId = Guid.NewGuid();
        ToDo? toDo = null;
        var command = new UpdateToDoCommand(toDoId, "first", "description", toDoTypeId);
        _toDoRepositoryMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<ToDoId>(), 
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(toDo);

        var handler = new UpdateToDoCommandHandler(
            _toDoRepositoryMock.Object,
            _toDoTypeMock.Object,
            _unitOfWorkMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.ToDos.NotFound(toDoId));
    }

    [Fact]
    public async Task Handle_Should_ReturnFailureResult_WhenPermissionTypeGuidNotFound()
    {
        // Arrange
        var toDoId = Guid.NewGuid();
        var typeId = Guid.NewGuid();
        var command = new UpdateToDoCommand(toDoId, "first2", "description", typeId);
        ToDoType? toDoType = null;
        ToDo toDo = new ToDo(new ToDoId(toDoId), "first2", "description", new ToDoType(new ToDoTypeId(Guid.NewGuid()), "b", 1));
        _toDoRepositoryMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<ToDoId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(toDo);

        _toDoTypeMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<ToDoTypeId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(toDoType);

        var handler = new UpdateToDoCommandHandler(
            _toDoRepositoryMock.Object,
            _toDoTypeMock.Object,
            _unitOfWorkMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.ToDosType.NotFound(typeId));
    }
}
