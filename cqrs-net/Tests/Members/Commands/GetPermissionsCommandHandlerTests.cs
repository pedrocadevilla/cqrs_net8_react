﻿using FluentAssertions;
using Moq;
using Application.Data;
using Domain.Shared;
using Domain.Errors;
using Application.ToDos.Get;
using Domain.ToDos;
using Domain.ToDoTypes;
using Application.ToDos.Models;

namespace Tests.Members.Commands;

public class GetTodosCommandHandlerTests
{
    private readonly Mock<IToDoRepository> _toDoRepositoryMock;
    private readonly Mock<IToDoTypeRepository> _toDoTypeRepositoryMock;
    private readonly Mock<IApplicationDbContext> _contextMock;

    public GetTodosCommandHandlerTests()
    {
        _toDoTypeRepositoryMock = new();
        _toDoRepositoryMock = new();
        _contextMock = new();
    }

    [Fact]
    public async System.Threading.Tasks.Task Handle_Should_ReturnFailureResult_WhenNoData()
    {
        // Arrange
        var command = new GetAllToDoQuery();
        _toDoRepositoryMock.Setup(
            x => x.GetAsync(
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(new List<ToDo>());

        var handler = new GetAllToDoQueryHandler(
            _contextMock.Object,
            _toDoRepositoryMock.Object
        );

        // Act
        Result<List<ToDoResponse>> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.ToDos.NoData);
    }

    [Fact]
    public async System.Threading.Tasks.Task Handle_Should_ReturnFailureResult_WhenIdDoesntMatch()
    {
        // Arrange
        var toDoId = Guid.NewGuid();
        ToDo? toDo = null;
        var command = new GetToDoQuery(new ToDoId(toDoId));
        _toDoRepositoryMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<ToDoId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(toDo);

        var handler = new GetToDoQueryHandler(
            _toDoTypeRepositoryMock.Object,
            _toDoRepositoryMock.Object
        );

        // Act
        Result<ToDoResponse> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.ToDos.NotFound(toDoId));
    }
}
