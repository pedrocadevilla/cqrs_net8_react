﻿using Domain.ToDos;
using Domain.ToDoTypes;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories;

internal sealed class ToDoTypeRepository : IToDoTypeRepository
{
    private readonly ApplicationDbContext _context;

    public ToDoTypeRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public Task<List<ToDoType>> GetAsync(CancellationToken cancellationToken)
    {
        return _context.ToDoTypes.ToListAsync(cancellationToken);
    }

    public Task<ToDoType?> GetByIdAsync(ToDoTypeId id, CancellationToken cancellationToken)
    {
        return _context.ToDoTypes.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
    }

    public void Add(ToDoType toDoType)
    {
        _context.ToDoTypes.Add(toDoType);
    }
}
