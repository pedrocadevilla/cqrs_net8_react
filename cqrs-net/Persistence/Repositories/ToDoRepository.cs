﻿using Domain.ToDos;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories;

internal sealed class ToDoRepository : IToDoRepository
{
    private readonly ApplicationDbContext _context;

    public ToDoRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public Task<ToDo?> GetByIdAsync(ToDoId id, CancellationToken cancellationToken)
    {
        return _context.ToDos.Include("ToDoType").SingleOrDefaultAsync(p => p.Id == id, cancellationToken);
    }

    public Task<List<ToDo>> GetAsync(CancellationToken cancellationToken)
    {
        return _context.ToDos.Include("ToDoType").ToListAsync(cancellationToken);
    }

    public void Add(ToDo toDo)
    {
        _context.ToDos.Add(toDo);
    }

    public void Update(ToDo toDo)
    {
        _context.ToDos.Update(toDo);
    }

    public void Remove(ToDo toDo)
    {
        _context.ToDos.Remove(toDo);
    }
}
