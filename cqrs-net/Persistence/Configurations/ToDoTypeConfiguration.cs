﻿using Domain.ToDoTypes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations;

internal class ToDoTypeConfiguration : IEntityTypeConfiguration<ToDoType>
{
    public void Configure(EntityTypeBuilder<ToDoType> builder)
    {
        builder.HasKey(pt => pt.Id);

        builder.Property(pt => pt.Id).HasConversion(
            toDoTypeId => toDoTypeId.Value,
            value => new ToDoTypeId(value));

        builder.Property(pt => pt.Name).HasMaxLength(100);

        builder.HasMany(tdt => tdt.Todo)
               .WithOne(td => td.ToDoType)
               .HasForeignKey(td => td.ToDoTypeId)
               .IsRequired();
    }
}
